#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*Josh Greenberg - 4/28/21*/
/*This program reads data from a text input file and computes some statistical analysis based off of that data.*/

#define STARTINGARRAYSIZE 20

/*Calculate the mean of an array of size len*/
double mean(float inputArray[], int len) {
  double sum = 0.0;
  for (int i = 0; i < len; ++i)
  {
    sum = sum + inputArray[i];
  }
  return sum / len;
}

/*Calculate the median of an array of size len*/
float median(float inputArray[], int len) {
  if (len % 2 == 0)
  {
    return (inputArray[(len / 2) - 1] + inputArray[len / 2]) / 2;
  } else {
    return inputArray[len / 2];
  }
}

/*Calculate the standard deviation of an array of size len with mean middle*/
double stddev(float inputArray[], int len, double middle) {
  double sum = 0.0;
  double temp = 0.0;
  for (int i = 0; i < len; ++i)
  {
    temp = inputArray[i] - middle;
    sum = sum + (temp * temp);
  }
  sum = sum / len;
  return sqrt(sum);
}

/*helper function for sorting an array of floats*/
int floatcomp(const void* elem1, const void* elem2)
{
    if(*(const float*)elem1 < *(const float*)elem2) {
      return -1;
    } else if (*(const float*)elem1 > *(const float*)elem2)
    {
      return 1;
    } else 
    {
      return 0;
    }
}

/*Main, driver function*/
int main(int argc, char const *argv[])
{
  
  if (argc != 2)
  { /*Check for proper input from the terminal*/
    printf("Cannot run without input file.\n");
  } else {
    FILE *inputFile = fopen(argv[1], "r"); /*open our file to read*/
    float *nums = malloc(STARTINGARRAYSIZE * sizeof(*nums)); /*initialize our array*/
    int size = STARTINGARRAYSIZE; /*total size of our array*/
    int length = 0; /*length of the array we are actually using*/
    float newNum = 0.0;
    while (!feof(inputFile)) { /*as long as there is still file to read*/
      if (length >= size)
      { /*First, check if we need to expand the size of our array*/
        size = size * 2; /*Double the size*/
        float *farray = malloc(size * sizeof(farray)); /*allocate memory for an array of that size*/
        for (int i = 0; i < length; ++i)
        { /*copy our old array to our new array*/
          farray[i] = nums[i];
        }
        free(nums); /*free the space used by the old array*/
        nums = farray; /*and change our pointer to the new array*/

      }
      /*Then read the next float in the file to a variable*/
      fscanf(inputFile, "%f", &newNum); 
      nums[length] = newNum; /*And assign the next slot in the array to that variable*/
      length++; /*then iterate to the next slot in the array*/
    }
    fclose(inputFile); /*Always close your files when you're done with them!*/

    /*Sort our new array of numbers to make them more useful.*/
    qsort(nums, length, sizeof(float), floatcomp);

    /*Print out our calculations as we make them*/
    printf("Results:\n--------\n");
    printf("Num Values: %13d\n", length);
    double avg = mean(nums, length); /*We need this for both the mean and the stddev, so only do calculation once*/
    printf("      mean: %13.3f\n", avg);
    printf("    median: %13.3f\n", median(nums, length));
    printf("    stddev: %13.3f\n", stddev(nums, length, avg));
    printf("Unused array capacity: %d\n", (size - length));
    
  }

  return 0;
}